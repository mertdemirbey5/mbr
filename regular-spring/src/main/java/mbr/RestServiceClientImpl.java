package mbr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class RestServiceClientImpl implements RestServiceClient {

    private static final Logger logger = LoggerFactory.getLogger(RestServiceClientImpl.class);

    @Value("${app.backend_url}")
    private String url;

    private final RestTemplate restTemplate;

    public RestServiceClientImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public CompletableFuture<String> callSymbolBackend(String symbol) {
        logger.info("Looking up symbol calling backend="+symbol);
        CompletableFuture<String> future
                = CompletableFuture.supplyAsync(() -> restTemplate.getForObject(url+symbol, String.class));
        return future;
    }
}