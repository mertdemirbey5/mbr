package mbr;

import java.util.concurrent.CompletableFuture;

public interface RestServiceClient {
    CompletableFuture<String> callSymbolBackend(String symbol);
}
