package mbr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class RestServiceProxy {
    @Value("${app.max_concurrent_request}")
    private Integer MAX_CONCURRENT_REQUEST;
    @Value("${app.defaultReturnValue}")
    private String defaultReturnValue;
    private AtomicInteger concurrentRequestCount = new AtomicInteger(0);
    @Autowired
    private RestServiceClient restServiceClient;

    public CompletableFuture<String> callBackend(String symbol){
        if(concurrentRequestCount.updateAndGet(value -> value < MAX_CONCURRENT_REQUEST ? value + 1 : value) < MAX_CONCURRENT_REQUEST){
            final CompletableFuture<String> stringCompletableFuture = restServiceClient.callSymbolBackend(symbol);
            concurrentRequestCount.decrementAndGet();
            return stringCompletableFuture;
        }
        else{
            return CompletableFuture.completedFuture(defaultReturnValue);
        }
    }
}
