package mbr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

@RestController
public class SymbolController {
    @Value("${app.defaultReturnValue}")
    private String defaultReturnValue;
    @Value("${app.backendWaitTimeInMiliSecond}")
    private Integer backendWaitTimeInMiliSecond;
    private static final LongAdder zeroReturnCount = new LongAdder();
    private static final LongAdder nonZeroReturnCount = new LongAdder();
    @Autowired
    private RestServiceProxy restServiceProxy;

    @RequestMapping(value="/{symbol}")
    public String symbolService(@PathVariable("symbol") String symbol) throws ExecutionException, InterruptedException {
        final CompletableFuture<String> stringCompletableFuture = restServiceProxy.callBackend(symbol);
        String returVal = stringCompletableFuture.completeOnTimeout(defaultReturnValue, backendWaitTimeInMiliSecond, TimeUnit.MILLISECONDS).get();
        if(!returVal .equals("0")){
            nonZeroReturnCount.increment();
        }
        else{
            zeroReturnCount.increment();
        }
        return returVal;
    }

    @RequestMapping(value="/statistic")
    public String statistic() throws ExecutionException, InterruptedException {
        String returnVal = "Zero return Count = "+ zeroReturnCount.longValue() + " no zero return count = "+ nonZeroReturnCount.longValue();
        zeroReturnCount.reset();
        nonZeroReturnCount.reset();
        return returnVal;
    }
}
