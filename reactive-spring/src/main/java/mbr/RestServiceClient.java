package mbr;

import reactor.core.publisher.Mono;

public interface RestServiceClient {
    Mono<String> callSymbolBackend(String symbol);
}
