package mbr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Service
public class RestServiceClientImpl implements RestServiceClient {
    private static final Logger logger = LoggerFactory.getLogger(RestServiceClientImpl.class);
    @Value("${app.backendWaitTimeInMiliSecond}")
    private Integer backendWaitTimeInMiliSecond;
    @Value("${app.defaultReturnValue}")
    private String defaultReturnValue;
    private static final String url = "http://localhost:3001";
    private WebClient webClient =  WebClient.create(url);

    public Mono<String> callSymbolBackend(String symbol) {
        return this.webClient.get().uri("/{symbol}", symbol).exchange().flatMap(response -> response.bodyToMono(String.class)).timeout(Duration.ofMillis(60), Mono.just(defaultReturnValue));
    }
}