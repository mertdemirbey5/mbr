package mbr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class SymbolController {

    @Autowired
    private RestServiceProxy restServiceProxy;

    @RequestMapping(value="/{symbol}")
    public Mono<String> symbolService(@PathVariable("symbol") String symbol){
        return restServiceProxy.callBackend(symbol);
    }
}
