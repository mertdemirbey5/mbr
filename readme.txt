Running The Application:

System requirements: Java 9 JRE

The application binary is a "fat" jar file which means no external java library is needed to run the application. If there is a "application.properties" files in the same directory of the application jar, it will overrides the application settings. The fat jar of the projects are in the bin directory.(mbr-symbol-springboot-reactive-0.1.0.jar and mbr-symbol-springboot-0.1.0.jar)

$java -jar mbr-symbol-springboot-reactive-0.1.0.jar will run the reactive implementation of the application

$java -jar mbr-symbol-springboot-0.1.0.jar will run the regular implementation.

Running Building The Application:
System requirements:Java 9 SDK, gradle 4.0+ and optional IDE(Intellij or Eclipse)

$gradlew build  command to build the application this command will generate a fat jar under build/libs directory.

Running The Application from the source
$gradlew buildRun command will run the application from the source.