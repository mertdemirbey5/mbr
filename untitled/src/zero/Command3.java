package zero;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
/**
 *
 * @author O
 */
public class Command3 {
    /**
     * The Command functional interface.
     */
    @FunctionalInterface
    public interface Command {
        public void apply();
    }

    /**
     * The CommandFactory class.
     */


    public static final class CommandFactory {
        private final Map<String, Command>	commands;

        private  CommandFactory() {
            commands = new HashMap<>();
        }

        public void addCommand(final String name, final Command command) {
            commands.put(name, command);
        }

        public void executeCommand(String name) {
            if (commands.containsKey(name)) {
                commands.get(name).apply();
            }
        }

        public void listCommands() {
            System.out.println("Enabled commands: " + commands.keySet().stream().collect(Collectors.joining(", ")));
        }

        /* Factory pattern */
        public static CommandFactory init() {
            final CommandFactory cf;
            cf = new CommandFactory();

            // Commands are added here using lambdas. It is also possible to dynamically add commands without editing the code.
            cf.addCommand("Light on", () -> System.out.println("Light turned on"));
            cf.addCommand("Light off", () -> System.out.println("Light turned off"));

            return cf;
        }
    }



    public static void main(final String[] arguments) {
        final CommandFactory cf = CommandFactory.init();

        cf.executeCommand("Light on");
        cf.listCommands();
    }
}