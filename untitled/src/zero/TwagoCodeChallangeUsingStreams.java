package zero;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class TwagoCodeChallangeUsingStreams {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        long[] sum = new long[x];

        IntStream.range(0, y).forEach(a -> {
                    int i = in.nextInt();
                    int j = in.nextInt();
                    int k = in.nextInt();
                    IntStream.range(i - 1, j).forEach(e -> sum[e] += k);
                }
        );

        System.out.println(Arrays.stream(sum).max().getAsLong());
    }
}
