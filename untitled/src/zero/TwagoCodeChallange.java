package zero;

import java.util.Arrays;
import java.util.Scanner;

public class TwagoCodeChallange {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        long[] sum = new long[x];
        for (int m = 0; m < y; m++) {
            int i = in.nextInt();
            int j = in.nextInt();
            int k = in.nextInt();
            for (int n = i - 1; n < j; n++) {
                sum[n] += k;
            }
        }
        System.out.println(Arrays.stream(sum).max().getAsLong());
    }
}
