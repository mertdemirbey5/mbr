public class Knight {
    private int name;
    private int health;
    public Knight(int name, int health) {
        this.name = name;
        this.health = health;
    }

    public boolean isDead(){
        return health <= 0;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void hit(int damage) {
        health -= damage;
    }
}
