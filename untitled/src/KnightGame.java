import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class KnightGame {
    public static void main(String args[]) {

        List<Knight> knights = new ArrayList<>();
        //initialize knights
        for (int i = 1; i <= 6; i++) {
            knights.add(new Knight(i, 100));
        }

        //game begins
        //for (int j = 0; ; j++) {
        while (knights.size() > 1){
            for (int i = 0; i < knights.size(); i++) {
                //hit
                Knight hitter = knights.get(i);
                int receiverIndex = (i + 1) % knights.size();
                Knight recevier = knights.get(receiverIndex);
                int damage = getRandomHit();
                recevier.hit(damage);
                System.out.println("Knight " + hitter.getName() + " hits Knight " + recevier.getName() +" with damage point "+damage);
                if (recevier.isDead()) {
                    System.out.println("Knight " + recevier.getName() + " is dead");
                    knights.remove(receiverIndex);
                    if(knights.size()==1){
                        //System.out.println("Knight " + knights.get(0).getName()+ " is the winner");
                        break;
                    }
                }
            }
        }
        //if(knights.size()==1){
        System.out.println("Knight " + knights.get(0).getName()+ " is the winner");
        //break;
    }


    public static int getRandomHit() {
        return ThreadLocalRandom.current().nextInt(1, 6 + 1);
    }

}
